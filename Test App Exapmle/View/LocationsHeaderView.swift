//
//  LocationsHeaderView.swift
//  Test App Exapmle
//
//  Created by artyom korotkov on 6/10/21.
//

import UIKit

class LocationsHeaderView: UIView {
    
    weak var delegate: LocationsHeaderViewDelegate? = nil
    
    let containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 20
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOffset = CGSize(width: 0, height: 10)
        view.layer.shadowOpacity = 0.1
        view.layer.shadowRadius = 3
        return view
    }()
    
    lazy var titleContainerLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = hexStringToUIColor(hex: "EDF3F4")
        label.clipsToBounds = true
        label.layer.cornerRadius = 10
        label.font = UIFont.systemFont(ofSize: 25)
        label.textColor = .black
        label.textAlignment = .center
        label.isUserInteractionEnabled = true
        label.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(labelTapped)))
        return label
    }()
    
    let titleContainerTextView: UITextView = {
        let textView = UITextView()
        textView.backgroundColor = hexStringToUIColor(hex: "EDF3F4")
        textView.clipsToBounds = true
        textView.layer.cornerRadius = 10
        textView.font = UIFont.systemFont(ofSize: 25)
        textView.textColor = .black
        textView.textAlignment = .center
        textView.isHidden = true
        return textView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(containerView)
        containerView.addSubview(titleContainerLabel)
        containerView.addSubview(titleContainerTextView)
        
        containerView.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.top.bottom.equalToSuperview().inset(10)
        }
        
        titleContainerLabel.snp.makeConstraints { (make) in
            make.edges.equalToSuperview().inset(15)
        }
        
        titleContainerTextView.snp.makeConstraints { (make) in
            make.edges.equalTo(titleContainerLabel)
        }
    }
    
    @objc func labelTapped() {
        delegate?.titleOfFolderLabelTapped()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

protocol LocationsHeaderViewDelegate: class {
    func titleOfFolderLabelTapped()
}
