//
//  ImageCollectionViewCell.swift
//  Test App Exapmle
//
//  Created by artyom korotkov on 6/10/21.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    
    weak var delegate: ImageCollectionViewCellDelegate? = nil
    var indexPath: IndexPath?
    
    let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 10
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    lazy var selectButton: UIButton = {
        let button = UIButton()
        button.layer.cornerRadius = 15
        button.backgroundColor = .white
        button.layer.borderWidth = 2
        button.layer.borderColor = hexStringToUIColor(hex: "CE6666").cgColor
        button.addTarget(self, action: #selector(selectButtonPressed), for: .touchUpInside)
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(imageView)
        imageView.addSubview(selectButton)
        
        imageView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        selectButton.snp.makeConstraints { make in
            make.top.right.equalToSuperview().inset(10)
            make.width.height.equalTo(30)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func selectButtonPressed() {
        delegate?.selectButtonPressed(for: self)
    }
    
}

protocol ImageCollectionViewCellDelegate: class {
    func selectButtonPressed(for cell: ImageCollectionViewCell)
}
