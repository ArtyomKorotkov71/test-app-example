//
//  LocationTableViewCell.swift
//  Test App Exapmle
//
//  Created by artyom korotkov on 6/10/21.
//

import UIKit
import SDWebImage

class LocationTableViewCell: UITableViewCell, UITextViewDelegate {
    
    weak var delegate: LocationTableViewCellDelegate? = nil
    var location: Location?
    
    let containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 20
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOffset = CGSize(width: 0, height: 10)
        view.layer.shadowOpacity = 0.1
        view.layer.shadowRadius = 3
        return view
    }()
    
    let imagesContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = hexStringToUIColor(hex: "EDF3F4")
        view.clipsToBounds = true
        view.layer.cornerRadius = 10
        return view
    }()
    
    lazy var titleOfLocationLabel: UILabel = {
        let label = UILabel()
        label.textColor = hexStringToUIColor(hex: "869495")
        label.text = ""
        label.font = UIFont.systemFont(ofSize: 20)
        label.isUserInteractionEnabled = true
        label.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(titleLabelTapped)))
        return label
    }()
    
    lazy var titleOfLocationTextView: UITextView = {
        let textView = UITextView()
        textView.textColor = hexStringToUIColor(hex: "869495")
        textView.text = ""
        textView.font = UIFont.systemFont(ofSize: 20)
        textView.isHidden = true
        textView.backgroundColor = .clear
        textView.delegate = self
        return textView
    }()
    
    lazy var addNewImageButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .black
        button.clipsToBounds = true
        button.layer.cornerRadius = 12.5
        button.setImage(UIImage(named: "add"), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        button.addTarget(self, action: #selector(addNewImage), for: .touchUpInside)
        button.isUserInteractionEnabled = true
        return button
    }()
    
    lazy var imagesCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 10
        layout.minimumInteritemSpacing = 10
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = .clear
        return collectionView
    }()
    
    lazy var deleteButton: UIButton = {
        let button = UIButton()
        button.layer.cornerRadius = 25
        button.layer.shadowColor = UIColor.black.cgColor
        button.layer.shadowOffset = CGSize(width: -10, height: 10)
        button.layer.shadowOpacity = 0.1
        button.layer.shadowRadius = 3
        button.setTitle("Удалить", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = hexStringToUIColor(hex: "CE6666")
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        button.layer.borderWidth = 2
        button.layer.borderColor = UIColor.white.cgColor
        button.isHidden = true
        button.addTarget(self, action: #selector(deleteButtonPressed), for: .touchUpInside)
        return button
    }()
    
    lazy var longPressGesture: UILongPressGestureRecognizer = {
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(goToEditingMode))
        longPress.minimumPressDuration = 1
        return longPress
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = .white
        contentView.isUserInteractionEnabled = true
        imagesCollectionView.addGestureRecognizer(longPressGesture)
        imagesCollectionView.register(ImageCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        self.addSubview(containerView)
        containerView.addSubview(imagesContainerView)
        imagesContainerView.addSubview(titleOfLocationLabel)
        imagesContainerView.addSubview(titleOfLocationTextView)
        imagesContainerView.addSubview(addNewImageButton)
        imagesContainerView.addSubview(imagesCollectionView)
        self.addSubview(deleteButton)
        
        containerView.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.top.equalToSuperview().inset(10)
            make.bottom.equalToSuperview().inset(30)
        }
        
        imagesContainerView.snp.makeConstraints { (make) in
            make.edges.equalTo(containerView).inset(15)
        }
        
        titleOfLocationLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(10)
            make.top.equalToSuperview().offset(10)
            make.height.equalTo(25)
            make.right.equalToSuperview().inset(40)
        }
        
        titleOfLocationTextView.snp.makeConstraints { (make) in
            make.edges.equalTo(titleOfLocationLabel)
        }
        
        addNewImageButton.snp.makeConstraints { (make) in
            make.centerY.equalTo(titleOfLocationLabel)
            make.right.equalToSuperview().inset(10)
            make.width.height.equalTo(25)
        }
        
        imagesCollectionView.snp.makeConstraints { (make) in
            make.top.equalTo(titleOfLocationLabel.snp.bottom).offset(10)
            make.left.right.equalToSuperview().inset(10)
            make.bottom.equalToSuperview()
        }
        
        deleteButton.snp.makeConstraints { make in
            make.centerY.equalTo(containerView.snp.bottom)
            make.height.equalTo(50)
            make.width.equalTo(200)
            make.centerX.equalToSuperview()
        }
    }
    
    func configureCellWith(location: Location) {
        self.location = location
        titleOfLocationLabel.text = location.title
        imagesCollectionView.reloadData()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func addNewImage() {
        guard let location = location else { return }
        delegate?.addNewImageToLocation(for: location)
    }
    
    @objc func titleLabelTapped() {
        delegate?.titleOfLocationLabelTapped(for: self)
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        guard let location = location else { return }
        delegate?.endedEditingTitleOfLocation(cell: self, location: location)
    }
    
    @objc func goToEditingMode() {
        guard let location = location else { return }
        if !location.isEditingMode {
            delegate?.goToEditingMode(for: location)
            UIImpactFeedbackGenerator().impactOccurred()
        }
        location.isEditingMode = true
        imagesCollectionView.reloadData()
    }
    
    @objc func deleteButtonPressed() {
        guard let location = location else { return }
        delegate?.deleteButtonPressed(for: location)
    }
}

//MARK: - Collection View

extension LocationTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, ImageCollectionViewCellDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return location?.imageNames.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ImageCollectionViewCell
        if let location = location {
            cell.imageView.sd_setImage(with: URL(string: location.imageNames[indexPath.row].1))
            cell.delegate = self
            cell.indexPath = indexPath
            if location.isEditingMode {
                cell.selectButton.isHidden = false
                if location.isImageNameSelected[indexPath.row] == true {
                    cell.selectButton.setImage(UIImage(named: "delete"), for: .normal)
                } else {
                    cell.selectButton.setImage(UIImage(), for: .normal)
                }
            } else {
                cell.selectButton.isHidden = true
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = ((self.frame.size.width - 30) - 10 * 4) / 3
        return CGSize(width: size, height: size)
    }
    
    func selectButtonPressed(for cell: ImageCollectionViewCell) {
        if let indexPathRow = cell.indexPath?.row {
            location?.isImageNameSelected[indexPathRow] = !(location?.isImageNameSelected[indexPathRow])!
        }
        imagesCollectionView.reloadData()
    }
}

protocol LocationTableViewCellDelegate: class {
    func addNewImageToLocation(for location: Location)
    func titleOfLocationLabelTapped(for cell: LocationTableViewCell)
    func endedEditingTitleOfLocation(cell: LocationTableViewCell, location: Location)
    func goToEditingMode(for location: Location)
    func deleteButtonPressed(for location: Location)
}
