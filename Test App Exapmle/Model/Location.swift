//
//  Location.swift
//  Test App Exapmle
//
//  Created by artyom korotkov on 6/10/21.
//

import Foundation

class Location {
    var title: String
    var imageNames: [(String, String)]
    var isImageNameSelected = [Bool]()
    var id: String
    var isEditingMode = false
    
    init(id: String, title: String, imageNames: [(String, String)]) {
        self.title = title
        self.imageNames = imageNames
        self.id = id
        isImageNameSelected = []
        isImageNameSelected.append(contentsOf: repeatElement(false, count: imageNames.count))
    }
}
