//
//  FireBaseManager.swift
//  Test App Exapmle
//
//  Created by artyom korotkov on 6/10/21.
//

import Foundation
import Firebase

class FirebaseManager {
    
    static let shared = FirebaseManager()
    
    var titleOfFolder: String = ""
    var locations: [Location] = []
    
    init() {
        Database.database().reference().child("titleOfFolder").observe(.value) { [weak self] (snapshot) in
            guard let self = self else { return }
            if let titleOfFolder = snapshot.value as? String {
                self.titleOfFolder = titleOfFolder
                NotificationCenter.default.post(name: .titleOfFolderUpdated, object: nil, userInfo: nil)
            }
        }
        
        Database.database().reference().child("locations").observe(.value) { [weak self] (snapshot) in
            guard let self = self else { return }
            var newArrayOfLocation = [Location]()
            guard snapshot.childrenCount > 0 else { return }
            for snapIndex in 0...(snapshot.childrenCount - 1) {
                let snap = snapshot.children.allObjects[Int(snapIndex)] as! DataSnapshot
                
                let id = snap.key as! String
                
                if let dictionary = snap.value as? [String: Any] {
                    var arrayOfImageNames = [(String, String)]()
                    let titleOfLocation = dictionary["title"] as! String
                    if let imageNames = dictionary["imageNames"] as? [String: String] {
                        for imageName in imageNames {
                            arrayOfImageNames.append((imageName.key, imageName.value))
                        }
                    }
                    arrayOfImageNames.sort(by: {$0 < $1})
                    let location = Location(id: id, title: titleOfLocation, imageNames: arrayOfImageNames)
                    newArrayOfLocation.append(location)
                }
            }
            newArrayOfLocation.sort { (location1, location2) -> Bool in
                location1.title < location2.title
            }
            self.locations = newArrayOfLocation
            NotificationCenter.default.post(name: .locationsUpdated, object: nil, userInfo: nil)
        }
    }
    
    func setNewTitleOfFolder(newTitle: String) {
        Database.database().reference().child("titleOfFolder").setValue(newTitle)
    }
    
    func addNewImageToLocationWithTitle(id: String, image: UIImage) {
        let imageName = NSUUID().uuidString
        let storageRef = Storage.storage().reference().child("locations_images").child("\(imageName).png")
        
        if let uploadData = image.jpegData(compressionQuality: 0.1) {
            
            storageRef.putData(uploadData, metadata: nil, completion: { (_, err) in

                print("Ohoooooooo")
                if err != nil {
                    print(err)
                    return
                }
                
                storageRef.downloadURL(completion: { (url, err) in
                    if let err = err {
                        print(err)
                        return
                    }
                    
                    guard let url = url else { return }
                    
                    Database.database().reference().child("locations").child(id).child("imageNames").child("\(imageName)").setValue(url.absoluteString)
                })

            })
        }
    }
    
    func addNewLocation() {
        var title = "Название локации"
        var currentAdditionToTitle: Int = 1
        while (isThereAnyLocationWithThisTitle(title: title)) {
            title = "Название локации \(currentAdditionToTitle)"
            currentAdditionToTitle += 1
        }
        Database.database().reference().child("locations").child(NSUUID().uuidString).updateChildValues(["title": title])
    }
    
    func isThereAnyLocationWithThisTitle(title: String) -> Bool {
        for location in locations {
            if location.title == title {
                return true
            }
        }
        return false
    }
    
    func updateTitleOfLocation(id: String, newName: String) {
        Database.database().reference().child("locations").child(id).child("title").setValue(newName)
    }
    
    func deleteImagesForLocationID(locationID: String, imageNames: [(String, String)], completionHandler: @escaping () -> ()) {
        for (index, imageName) in imageNames.enumerated() {
            Database.database().reference().child("locations").child(locationID).child("imageNames").child(imageName.0).removeValue { error, ref in
                if index == imageNames.count {
                    completionHandler()
                }
            }
        }
    }
    
}
