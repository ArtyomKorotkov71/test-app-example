//
//  CustomTabBarViewController.swift
//  Test App Exapmle
//
//  Created by artyom korotkov on 6/10/21.
//

import UIKit

class CustomTabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let settingsViewController = UINavigationController(rootViewController: SettingsViewController())
        settingsViewController.tabBarItem.image = UIImage(named: "Settings")
        settingsViewController.tabBarItem.title = ""
        settingsViewController.tabBarItem.badgeColor = .black
        
        let moneyViewController = UINavigationController(rootViewController: MoneyViewController())
        moneyViewController.tabBarItem.image = UIImage(named: "money")
        moneyViewController.tabBarItem.title = ""
        moneyViewController.tabBarItem.badgeColor = .black
        
        let listOfShootingsViewController = UINavigationController(rootViewController: ListOfShootingsViewController())
        
        let moodBoardViewController = UINavigationController(rootViewController: MoodBoardViewController())
        moodBoardViewController.tabBarItem.image = UIImage(named: "moodboard")
        moodBoardViewController.tabBarItem.title = ""
        moodBoardViewController.tabBarItem.badgeColor = .black
        
        let locationViewController = UINavigationController(rootViewController: LocationViewController())
        locationViewController.tabBarItem.image = UIImage(named: "Location")
        locationViewController.tabBarItem.title = ""
        locationViewController.tabBarItem.badgeColor = .black
        
        tabBarItem.badgeColor = .black
        tabBar.tintColor = .black
        tabBar.barTintColor = .white
        tabBar.isTranslucent = false
        viewControllers = [settingsViewController, moneyViewController, listOfShootingsViewController, moodBoardViewController, locationViewController]
        
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "Clock"), for: .normal)
        button.backgroundColor = .clear
        button.addTarget(self, action: #selector(openListOfShootingsVC), for:.touchUpInside)
        view.addSubview(button)
        
        button.snp.makeConstraints { (make) in
            make.centerY.equalTo(tabBar.snp.top)
            make.width.height.equalTo(100)
            make.centerX.equalToSuperview()
        }
        
        selectedIndex = 4
    }
    
    @objc func openListOfShootingsVC() {
        selectedIndex = 2
    }

}
