//
//  LocationViewController.swift
//  Test App Exapmle
//
//  Created by artyom korotkov on 6/10/21.
//

import UIKit
import Firebase
import PhotosUI
import SVProgressHUD

class LocationViewController: UIViewController {
    
    var titleOfLocationToAddImages: String = ""
    
    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.delaysContentTouches = false
        tableView.allowsSelection = false
        tableView.backgroundColor = .white
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 40, right: 0)
        return tableView
    }()
    
    lazy var locationsHeaderView: LocationsHeaderView = {
        let headerView = LocationsHeaderView()
        headerView.delegate = self
        return headerView
    }()
    
    lazy var addNewLocationButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .black
//        button.clipsToBounds = true
        button.layer.cornerRadius = 30
        button.setImage(UIImage(named: "add"), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 2, left: 2, bottom: 2, right: 2)
        button.addTarget(self, action: #selector(addNewLocation), for: .touchUpInside)
        button.isUserInteractionEnabled = true
        
        button.layer.shadowColor = UIColor.black.cgColor
        button.layer.shadowOffset = CGSize(width: -5, height: 10)
        button.layer.shadowOpacity = 0.1
        button.layer.shadowRadius = 3
        
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        locationsHeaderView.titleContainerLabel.text = FirebaseManager.shared.titleOfFolder
        tableView.register(LocationTableViewCell.self, forCellReuseIdentifier: "cell")
        setupView()
        addFirebaseObservers()
        hideKeyboardWhenTappedAround()
        setupKeyBoardMethods()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupNavBar()
    }
    
    func setupKeyBoardMethods() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidHide), name: UIResponder.keyboardDidHideNotification, object: nil)
    }
    
    func addFirebaseObservers() {
        NotificationCenter.default.addObserver(forName: .locationsUpdated, object: nil, queue: nil) { [weak self] (notificationObject) in
            guard let self = self else { return }
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.tableView.reloadData()
                SVProgressHUD.dismiss()
            }
        }
        
        NotificationCenter.default.addObserver(forName: .titleOfFolderUpdated, object: nil, queue: nil) { [weak self] (notificationObject) in
            guard let self = self else { return }
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.locationsHeaderView.titleContainerLabel.text = FirebaseManager.shared.titleOfFolder
            }
        }
    }
    
    func setupNavBar() {
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = UIColor(white: 0.98, alpha: 1)
        navigationController?.navigationBar.shadowImage = UIImage()
        
        let containerView = UIView()
        
        let titleImageView = UIImageView()
        titleImageView.image = UIImage(named: "navBarBack")
        titleImageView.contentMode = .scaleAspectFit

        let titleLabel = UILabel()
        titleLabel.text = "ЛОКАЦИИ"
        titleLabel.font = UIFont.systemFont(ofSize: 30, weight: .light)
        titleLabel.textAlignment = .center
        titleLabel.textColor = .black

        containerView.addSubview(titleImageView)
        containerView.addSubview(titleLabel)

        titleImageView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }

        titleLabel.snp.makeConstraints { (make) in
            make.top.bottom.equalToSuperview().inset(5)
            make.left.right.equalToSuperview()
        }
        
        navigationItem.titleView = containerView
    }
    
    func setupView() {
        view.backgroundColor = .white
        view.addSubview(locationsHeaderView)
        view.addSubview(tableView)
        view.addSubview(addNewLocationButton)
        
        locationsHeaderView.snp.makeConstraints { (make) in
            make.top.equalTo(view.safeAreaLayoutGuide)
            make.left.right.equalTo(view)
            make.height.equalTo(100)
        }
        
        tableView.snp.makeConstraints { (make) in
            make.top.equalTo(locationsHeaderView.snp.bottom)
            make.left.right.equalTo(view)
            make.bottom.equalTo(view.safeAreaLayoutGuide)
        }
        
        addNewLocationButton.snp.makeConstraints { (make) in
            make.bottom.equalTo(view.safeAreaLayoutGuide).inset(50)
            make.right.equalToSuperview().inset(20)
            make.width.height.equalTo(60)
        }
    }
    
    @objc func addNewLocation() {
        SVProgressHUD.show()
        FirebaseManager.shared.addNewLocation()
    }
}

//MARK: - Table View

extension LocationViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return FirebaseManager.shared.locations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! LocationTableViewCell
        let location = FirebaseManager.shared.locations[indexPath.row]
        cell.configureCellWith(location: location)
        cell.delegate = self
        if FirebaseManager.shared.locations[indexPath.row].isEditingMode {
            cell.deleteButton.isHidden = false
        } else {
            cell.deleteButton.isHidden = true
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return countHeightForLocationAt(index: indexPath.row)
    }
    
    func countHeightForLocationAt(index: Int) -> CGFloat {
        let sizeOfSingleImage = ((view.frame.width-30)-10*4)/3.0
        let numberOfImages = FirebaseManager.shared.locations[index].imageNames.count
        var numberOfRows = numberOfImages / 3
        if (numberOfImages % 3) != 0 {
            numberOfRows += 1
        }
        let heightForCollectionView = numberOfRows * Int(sizeOfSingleImage) + max(numberOfRows - 1, 0) * 5
        return CGFloat(heightForCollectionView + 130)
    }
}


// MARK: - Editing Labels Methods

extension LocationViewController: LocationsHeaderViewDelegate, LocationTableViewCellDelegate {
    func goToEditingMode(for location: Location) {
        let indexOfLocationWithThisId = FirebaseManager.shared.locations.firstIndex { loc in
            return loc.id == location.id
        }
        guard let safeIndexOfLocationWithThisId = indexOfLocationWithThisId else { return }
        FirebaseManager.shared.locations[safeIndexOfLocationWithThisId].isEditingMode = true
        tableView.reloadData()
    }
    
    @objc func keyboardDidHide() {
        locationsHeaderView.titleContainerLabel.isHidden = false
        locationsHeaderView.titleContainerTextView.isHidden = true
        if (locationsHeaderView.titleContainerTextView.text.count != 0) {
            locationsHeaderView.titleContainerLabel.text = locationsHeaderView.titleContainerTextView.text
        }
        FirebaseManager.shared.setNewTitleOfFolder(newTitle: locationsHeaderView.titleContainerLabel.text ?? "Название")
    }
    
    func titleOfFolderLabelTapped() {
        locationsHeaderView.titleContainerLabel.isHidden = true
        locationsHeaderView.titleContainerTextView.isHidden = false
        locationsHeaderView.titleContainerTextView.text = locationsHeaderView.titleContainerLabel.text
        locationsHeaderView.titleContainerTextView.becomeFirstResponder()
    }
    
    func titleOfLocationLabelTapped(for cell: LocationTableViewCell) {
        cell.titleOfLocationLabel.isHidden = true
        cell.titleOfLocationTextView.isHidden = false
        cell.titleOfLocationTextView.text = cell.titleOfLocationLabel.text
        cell.titleOfLocationTextView.becomeFirstResponder()
    }
    
    func endedEditingTitleOfLocation(cell: LocationTableViewCell, location: Location) {
        if FirebaseManager.shared.isThereAnyLocationWithThisTitle(title: cell.titleOfLocationTextView.text) {
            cell.titleOfLocationLabel.isHidden = false
            cell.titleOfLocationTextView.isHidden = true
            let alert = UIAlertController(title: "Not valid name", message: "This name was already used. Please select some other name", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        let locationId = location.id
        cell.titleOfLocationLabel.isHidden = false
        cell.titleOfLocationTextView.isHidden = true
        if (cell.titleOfLocationTextView.text.count != 0) {
            cell.titleOfLocationLabel.text = cell.titleOfLocationTextView.text
        }
        FirebaseManager.shared.updateTitleOfLocation(id: locationId, newName: cell.titleOfLocationLabel.text ?? "")
    }
    
    func deleteButtonPressed(for location: Location) {
        SVProgressHUD.show()
        var imageNamesForDeletion = [(String, String)]()
        for (index, imageName) in location.imageNames.enumerated() {
            if (location.isImageNameSelected[index]) {
                imageNamesForDeletion.append(imageName)
            }
        }
        FirebaseManager.shared.deleteImagesForLocationID(locationID: location.id, imageNames: imageNamesForDeletion) {
            SVProgressHUD.dismiss()
        }
    }
}

//MARK: - Picker

extension LocationViewController: PHPickerViewControllerDelegate {
    
    func addNewImageToLocation(for location: Location) {
        let locationId = location.id
        titleOfLocationToAddImages = locationId
        var config = PHPickerConfiguration()
        config.selectionLimit = 5
        config.filter = PHPickerFilter.images

        let pickerViewController = PHPickerViewController(configuration: config)
        pickerViewController.delegate = self
        self.present(pickerViewController, animated: true, completion: nil)
    }
    
    func picker(_ picker: PHPickerViewController, didFinishPicking results: [PHPickerResult]) {
        picker.dismiss(animated: true, completion: nil)
       
        for result in results {
            SVProgressHUD.show()
            result.itemProvider.loadObject(ofClass: UIImage.self, completionHandler: { (object, error) in
                if let image = object as? UIImage {
                    FirebaseManager.shared.addNewImageToLocationWithTitle(id: self.titleOfLocationToAddImages, image: image)
                }
            })
        }
    }
}
